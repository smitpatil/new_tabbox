#pragma once

#include <KQuickAddons/ConfigModule>

class TaskSwitcherKcm : public KQuickAddons::ConfigModule {
    Q_OBJECT

public:
    TaskSwitcherKcm(QObject* parent, const QVariantList& args);
    ~TaskSwitcherKcm();

    void save() override;
    void defaults() override;
    void load() override;

    Q_INVOKABLE QString testImgPath(QString img) const;

Q_SIGNALS:

private:
    bool syncAll;
};

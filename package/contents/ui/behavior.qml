import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

//import org.kde.kirigami 2.14 as Kiri
import org.kde.kirigami 2.14

ScrollablePage	{
    title: "Behavior"

    actions {
        contextualActions: root.globalActions
    }

//    Layout.fillWidth: true


    FormLayout {
//        anchors.fill: parent

        CheckBox {
            text: "Show selected Windows"
        }


        CheckBox {
            text: 'Incluede "Show Desktop" icon'
        }

        CheckBox {
            text: "Only one window per application"
        }

        ComboBox {
            FormData.label: "Sort Order"
            model: ["Recently Used","Stacking Order"]
        }

//        Kiri.Separator {
//            Kiri.FormData.label: "Filter window by"
//            Kiri.FormData.isSection: true
//        }

        component CheckBoxRadio : Column {

            property alias text: ch_toggle.text
            property alias text1: ch_1.text
            property alias text2: ch_2.text

            CheckBox {
                id: ch_toggle
            }

            Row {
                enabled: ch_toggle.checked
                RadioButton{
                    id: ch_1
                }
                RadioButton{
                    id: ch_2
                }
            }

        }

        Separator {
            FormData.label: "Filter windows by"
            FormData.isSection: true
        }

        CheckBoxRadio {
            text: "Virtual desktops"
            text1: "Current desktop"
            text2: "All other desktop"
        }
        CheckBoxRadio {
            text: "Activities desktops"
            text1: "Current activity"
            text2: "All other activity"
        }
        CheckBoxRadio {
            text: "Minimization desktops"
            text1: "Current desktop"
            text2: "All other desktop"
        }

        Separator {
            FormData.label: "Shortcuts"
            FormData.isSection: true
        }

        component ShortcutSpecifier :  RowLayout {
            id : lay
            property string label
            property string shortcut_text


            Layout.fillWidth: true
            FormData.label: lay.label

            Button {
                icon.name: "configure-shortcuts"
                text: lay.shortcut_text
                Layout.preferredWidth: Units.gridUnit * 10
            MnemonicData.enabled: false
                focus: true
            }

            Button {
                icon.name: "edit-clear"
            }

        }

        Label {
            text: "All Windows"
        }

        ShortcutSpecifier {
            label : "Forward"
            shortcut_text: "Meta+Tab"
        }
        ShortcutSpecifier {
            label : "Reverse"
            shortcut_text: "Meta+~"
        }

        Label {
            text: "Current Application's windows"
        }

        ShortcutSpecifier {
            label : "Forward"
            shortcut_text: "Meta+Tab"
        }
        ShortcutSpecifier {
            label : "Reverse"
            shortcut_text: "Meta+~"
        }


    }
}

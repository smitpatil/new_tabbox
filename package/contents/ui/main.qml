import QtQuick 2.12
import QtQuick.Controls 2.12

import org.kde.kirigami 2.15 as Kiri
import org.kde.kcm 1.2
import QtQuick.Layouts 1.15

SimpleKCM {
//Kiri.ApplicationWindow {
    id: rootElement

//    width: 500
//    height: 700

    property list<Kiri.Action> gActions: [
        Kiri.Action {
            displayComponent: CheckBox {
                text: "Main"
            }
        },
        Kiri.Action {
            displayComponent: CheckBox {
                text: "Alternative"
            }
        }
    ]


    Component.onCompleted: kcm.push("appearance.qml")

}

import QtQuick 2.15
import org.kde.kcm 1.4 as KCM
import org.kde.kirigami 2.14 as Kiri
import org.kde.newstuff 1.81 as NewStuff
import QtQuick.Layouts 1.15

KCM.GridViewKCM {
    title: "Appearance"


    actions {
        contextualActions:  gActions
    }

    id: grid
    view.model: ListModel {
        ListElement {
            display : "Cover Switch"
            author : "firelord zuko"
            thumbnail : "../img/cover_switch.png"
        }
        ListElement {
            display: "Breeze Dark"
            author : "Waterlord Naruto"
            thumbnail : "../img/breeze_dark.png"
        }
        ListElement {
            display : "Cover Switch"
            author : "firelord zuko"
            thumbnail : "../img/cover_switch.png"
        }
        ListElement {
            display: "Breeze Dark"
            author : "Waterlord Naruto"
            thumbnail : "../img/breeze_dark.png"
        }
        ListElement {
            display : "Cover Switch"
            author : "firelord zuko"
            thumbnail : "../img/cover_switch.png"
        }
        ListElement {
            display: "Breeze Dark"
            author : "Waterlord Naruto"
            thumbnail : "../img/breeze_dark.png"
        }

    }


    view.delegate: KCM.GridDelegate {
        id: delegate
        text: model.display
        subtitle: model.author
        thumbnailAvailable: true
        thumbnail: Image {
            anchors.fill: parent
            source: model.thumbnail
            fillMode: Image.PreserveAspectFit
            smooth: true
        }
        actions: [
            Kiri.Action {
                iconName: "documentinfo"
            },
            Kiri.Action {
                iconName: "view-preview"
            }
        ]
        onClicked: grid.view.currentIndex = model.index

    }
    //    }

    //    header :  Kiri.ActionToolBar {
    //        flat: false
    //        alignment: Qt.AlignRight
    //        actions: mainWindow.globalActions
    //    }

    footer: Kiri.ActionToolBar {
        flat: false
        alignment: Qt.AlignRight
        actions: [
            Kiri.Action {
                text: "Behavior"
                icon.name: "settings-configure"
                onTriggered: {
                    kcm.push("behavior.qml")
//                    pageStack.push("behavior.qml")
                }
            },
            Kiri.Action {
                text: "Install From File"
                icon.name: "document-import"
            },
            NewStuff.Action {
                text: "Get New Task Switchers..."
                icon.name: "get-hot-new-stuff"
            }
        ]
    }
}

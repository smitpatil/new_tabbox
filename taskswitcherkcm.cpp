
#include "taskswitcherkcm.hpp"

#include <KAboutData>
#include <KLocalizedString>
#include <KPluginFactory>

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>

#include <QDBusPendingCallWatcher>

#include <QDate>

#include <QDebug>

K_PLUGIN_CLASS_WITH_JSON(TaskSwitcherKcm, "metadata.json")

inline static KAboutData* taskSwitcherKcmAboutData()
{
    KAboutData* data = new KAboutData;
    data->setLicense(KAboutLicense::GPL_V2);

    data->setComponentName(QStringLiteral("TaskSwitcher"));
    data->setDisplayName(QStringLiteral("TaskSwitcher"));
    data->setVersion("0.1");
    data->setCopyrightStatement(
        QStringLiteral("Copyright (C) 2022-%1 Smit S. Patil").arg(QDate::currentDate().year()));

    data->addAuthor(
        i18nc("@info:credit", "Smit S. Patil"),
        QStringLiteral("Developer and Maintainer"),
        QStringLiteral("smit17av@gmail.com"),
        QStringLiteral("https://smit17.netlify.app/"));

    return data;
}

TaskSwitcherKcm::TaskSwitcherKcm(QObject* parent, const QVariantList& args)
    : KQuickAddons::ConfigModule(parent, args)
{

    setAboutData(taskSwitcherKcmAboutData());
    setButtons(Apply | Default);
}

TaskSwitcherKcm::~TaskSwitcherKcm() {}


// on reset button
void TaskSwitcherKcm::load()
{
}

// on save button
void TaskSwitcherKcm::save()
{
}

// on default button
void TaskSwitcherKcm::defaults()
{
}


QString TaskSwitcherKcm::testImgPath(QString img) const
{
    return QStandardPaths::locate(
        QStandardPaths::StandardLocation::GenericDataLocation,
        QStringLiteral("taskswitcherkcm/pics/%1").arg(img));
}

#include "taskswitcherkcm.moc"

import QtQuick 2.15
import QtQuick.Controls 2.15

import org.kde.kcm 1.4 as KCM
import org.kde.kirigami 2.14 as Kiri
import org.kde.newstuff 1.81 as NewStuff

import QtQml.Models 2.15

KCM.SimpleKCM {

    id: mainWindow

    width: 600
    height: 700

//    visible: true


    property list<Kiri.Action> globalActions :  [
            Kiri.Action {
                text: "Main"
                displayComponent: RadioButton {
                    text: "Main"
                }
            },
            Kiri.Action {
                text: "Alternative"
                displayComponent: RadioButton {
                    text: "Alternative"
                }
            }

        ]

    component TaskSwitcherList : Kiri.Page {
        title: "Task Switcher"
        ListView {
            anchors.fill: parent
            delegate: Kiri.BasicListItem {
                label: model.label
                subtitle: model.task
                onClicked: pageStack.push("qrc:/appearance.qml")
            }
            model : ListModel {
                ListElement {
                    label: "Main"
                    task: "Covert Switcher"
                    shortcut: "Meta+Tab"
                }
                ListElement {
                    label: "Alternative"
                    task: "Flip Switch"
                }
            }
        }
    }

    Component.onCompleted: {
        kcm.push("qrc:/appearance.qml")
    }

//    TaskSwitcherList {id : ls }


//    pageStack : Kiri.PageRow {
//        anchors.fill: parent
//        id: pageRow

////        initialPage: TaskSwitcherList{}
//    }

//    pageStack.initialPage: TaskSwitcherList{}
//    pageStack.interactive: false

//    Timer {
//        running: true
//        interval: 100
//        onTriggered:  pageStack.push("qrc:/appearance.qml")
//    }

//    Timer {
//        interval: 1000
//        running: true
//        onTriggered:  pageStack.push("qrc:/behavior.qml")
//    }

//    Component.onCompleted: {
//        pageStack.push("qrc:/appearance.qml")
//        pageStack.push("qrc:/behavior.qml")
//    }


}

